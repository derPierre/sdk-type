export type EloPlayer<P> = P & {
	/**
	 * index of the player, the player who created the game has index 0
	 */
	index: number
	/**
	 * user name of the player, should only be used in the ui
	 */
	name: string
	/**
	 * if the player is a bot the level is set
	 */
	aiLevel: number
	/**
	 * flag used to allow a user to send moves.
	 * Change from false to true to notify player
	 */
	active: boolean
	/**
	 * color that the app should use to display this player
	 */
	color: string
	/**
	 * flag that should be set whenever the player left the game
	 */
	quit: boolean
	/**
	 * @deprecated use quit instead
	 */
	timeout: boolean
	/**
	 * should be set as soon as the player interacted with the game.
	 * Leads to a loss if the player leaves
	 */
	committed: boolean
	/**
	 * displayed in the elo result scene
	 * 1 is for the winner
	 */
	rank: number
	/**
	 * displayed in the elo result scene
	 */
	score: number
	/**
	 * number of the team
	 */
	team: number
	/**
	 * all moves that are allowed for the player in the current state
	 */
	validMoveTypes: string[]
	/**
	 * next player, first player is the next one from the last
	 */
	next: EloPlayer<P>
	/**
	 * previous player, last player is the previous one from the first
	 */
	 previous: EloPlayer<P>
}

export type PlayerList<P> = {
	length: number
	get(index: number): EloPlayer<P>
	all(): EloPlayer<P>[]
	random(): EloPlayer<P>
	after(player: EloPlayer<P>): EloPlayer<P>
	/**
	 * Executes a provided function once for every ´Player´ in this ´PlayerList´.
	 *
	 * @param callback Function to execute for each ´Player´
	 * @param thisArg Value to use as ´this´ when executing ´callback´
	 */
	forEach(callback: (player: EloPlayer<P>) => void, thisArg?: any): void
	/**
	 * Creates and returns a new array with the results of calling the
	 * provided ´callback´ function on every player in this ´PlayerList´.
	 *
	 * @param callback Function that produces an element of the new ´Array´
	 * @param thisArg Value to use as ´this´ when executing ´callback´
	 */
	map(callback: (player: EloPlayer<P>) => any, thisArg?: any): any[]
}

export type EloEvent<P, MoveType> = {
	type: string
	player: EloPlayer<P>
	move?: GenericMove<MoveType>
}

export type GameJSON = {
	id: string
	type: string
	version: string
	minPlayerCount: number
	maxPlayerCount: number
	nativeSize: number
	eloCore: string
	ranking: string
	highscore: string
	cooperative: boolean
	scoring: string
	series: string
	textColor: string
	backgroundColor: string
	secondaryBackgroundColor: string
	journeyHighlightColor: string
	journeyColor: string
	playerColors: string[]
	playerTextColors: string[]
	playerNeutralColor: string[]
	keywords: string[]
	tags: string[]
	localizations: any
	publisherName: string
	team: {
		name: string
		roles: string[]
	}[]
	startDate: string
}

export type Environment = {
	simulator: boolean
}

export type GameManagerFacade<M, P, MoveType, CustomSettings = {}> = {
	players: PlayerList<P>
	player: EloPlayer<P>
	model: M
	environment: Environment
	scaleFactor: number
	locale: string
	game: GameJSON
	showTutorial: boolean
	settings: Settings<CustomSettings>
	addEventListener(type: string, listener: (event: EloEvent<P, MoveType>) => void): void
	setGameReady(): void
	createMove(type: MoveType): GenericMove<MoveType>
	/**
	 * Invokes the move handler which has been registered for the export type of the provided move.
	 */
	processMove(move: GenericMove<MoveType>): void
	sendMove(move: GenericMove<MoveType>): void
	acknowledgeMove(move: GenericMove<MoveType>): void
	acknowledgeResult(): void
	displayMessage(message: string, variables: any[], fallback: string, level: EloFacade<M, P, MoveType, CustomSettings>["MESSAGE_LEVEL_REGULAR"] | EloFacade<M, P, MoveType, CustomSettings>["MESSAGE_LEVEL_HINT"] | EloFacade<M, P, MoveType, CustomSettings>["MESSAGE_LEVEL_NOTIFICATION"]): void
	selectPlayer(): void
	registerSound(name: string, file: string, type: any): void
	playSound(
		name: string,
		options: {
			volume?: number
			loop?: number
		}
	): SoundInterface
	setFocusMode(value: boolean): void
	sendDictionaryRequest(request: { words: string[]; language?: string }, response: (result: boolean[], trackingID: number) => void): number
	setMoveDispatchPaused(paused: boolean): void
	takeScreenshot(result: boolean): void
}

export type SoundInterface = {
	stop(): void
	addEventListener(type: any, callback: () => void): void
}

export type EloLogic<M, P, MoveType, CustomSettings> = {
	players: PlayerList<P>
	backendMode: boolean
	environment: Environment
	game: GameJSON
	settings?: Settings<CustomSettings>
	model: M
	setModel(model: M): void
	registerMoveHandler(name: string, call: (move: GenericMove<MoveType>) => void): void
	registerAIMoveGenerator(ai: (player: EloPlayer<P>) => GenericMove<MoveType>): void
	createMove(type: string): GenericMove<MoveType>
	addBackendMove(move: GenericMove<MoveType>): void
	setGameFinished(): void
	getRandom(): number

	//addDelayedBackendMove: 
	//reportAchievementProgress: 
	//timers: 
}

export type Settings<CustomSettings> = CustomSettings & {
	elo_gameMode?: 'tournament'
	elo_seed?: number
	elo_run_index?: number
	elo_attempt_index?: number
}

export type Logger = {
	debug(message: string): void
	info(message: string): void
	warn(message: string): void
	error(message: string): void
}

export type EloFacade<M, P, MoveType, CustomSettings> = {
	EVENT_GAME_FINISHED: 'finished'
	EVENT_HEADER_BUTTON_CLICK: 'menuButtonClick'
	EVENT_MOVE: 'move'
	EVENT_MOVE_ACHIEVEMENT: 'move.elo_achievement'
	EVENT_MOVE_DELAY: 'move.elo_delay'
	EVENT_MOVE_DICTIONARY_REQUEST: 'move.elo_dictionaryRequest'
	EVENT_MOVE_DICTIONARY_RESPONSE: 'move.elo_dictionaryResponse'
	EVENT_MOVE_QUIT: 'move.quit'
	EVENT_MOVE_QUIZ_REQUEST: 'move.elo_quizRequest'
	EVENT_MOVE_QUIZ_RESPONSE: 'move.elo_quizResponse'
	EVENT_MOVE_STORE_REQUEST: 'move.elo_storeRequest'
	EVENT_MOVE_STORE_RESPONSE: 'move.elo_storeResponse'
	EVENT_MOVE_TIMEOUT: 'move.timeout'
	EVENT_MOVE_TIMESTART: 'move.elo_timestart'
	EVENT_OVERVIEW: 'overview'
	EVENT_OVERVIEW_HIDE: 'overview.hide'
	EVENT_OVERVIEW_SHOW: 'overview.show'
	EVENT_QUIT_MOVE_IS_WAITING_IN_QUEUE: 'quitMoveIsWaitingInQueue'
	EVENT_SELECT_PLAYER: 'selectPlayer'
	EVENT_SOUND_DID_COMPLETE: 'sound.complete'
	EVENT_SOUND_DID_LOOP: 'sound.loop'
	EVENT_STATE: 'state'
	EVENT_STATE_PAUSE: 'state.pause'
	EVENT_STATE_RUN: 'state.run'
	EVENT_TIMEOUT_MOVE_IS_WAITING_IN_QUEUE: 'timeoutMoveIsWaitingInQueue'
	EVENT_UPDATE_PLAYER: 'updatePlayer'
	HEADER_BUTTON_POSITION_LEFT: 'left'
	MOVE_ACHIEVEMENT: 'elo_achievement'
	MOVE_DELAY: 'elo_delay'
	MOVE_DICTIONARY_REQUEST: 'elo_dictionaryRequest'
	MOVE_DICTIONARY_RESPONSE: 'elo_dictionaryResponse'
	MOVE_QUIT: 'quit'
	MOVE_QUIZ_REQUEST: 'elo_quizRequest'
	MOVE_QUIZ_RESPONSE: 'elo_quizResponse'
	MOVE_STORE_REQUEST: 'elo_storeRequest'
	MOVE_STORE_RESPONSE: 'elo_storeResponse'
	MOVE_TIMEOUT: 'timeout'
	MOVE_TIMESTART: 'elo_timestart'
	QUIZ_GET_MULTIPLE_CHOICE_QUESTIONS: 'getMultipleChoiceQuestions'
	SOUND_TYPE_EFFECT: 'effect'
	SOUND_TYPE_MUSIC: 'music'
	MESSAGE_LEVEL_REGULAR: 'regular'
	MESSAGE_LEVEL_HINT: 'hint'
	MESSAGE_LEVEL_NOTIFICATION: 'notification'

	logger: Logger
	createManager(callback: (manager: GameManagerFacade<M, P, MoveType, CustomSettings>) => void): void
	createLogic(func: (logic: EloLogic<M, P, MoveType, CustomSettings>) => void): void
}
export type GenericMove<MoveType> = {
	creationDate: Date
	type: MoveType
}

export type BackendMove<MoveType> = GenericMove<MoveType> & {}
export type PlayerMove<MoveType, P> = GenericMove<MoveType> & {
	player: EloPlayer<P>
}
